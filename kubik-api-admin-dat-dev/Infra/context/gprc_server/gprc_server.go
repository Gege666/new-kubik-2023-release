package grpc_server

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"kubik-api-admin/config"
	"kubik-api-admin/infra/context/handler"
	"kubik-api-admin/infra/context/repository"
	"kubik-api-admin/infra/context/service"
	"kubik-api-admin/infra/db"
	"net"
	"os"
)

var (
	grpcCMD = &cobra.Command{
		Use:   "serve-grpc",
		Short: "Run grpc_server server",
		Long:  "Kubik API Admin",
		RunE:  runGRPC,
	}
)

func ServeGRPC() *cobra.Command {
	return grpcCMD
}

func initializeListener(cfg *config.Config) (net.Listener, *grpc.Server) {
	// Open and Listen Server to configured Address
	listener, err := net.Listen("tcp", cfg.Server.Addr)
	if err != nil {
		log.Panicf("Failed listen port %s => %v", cfg.Server.Addr, err)
		os.Exit(0)
	}

	log.Printf("RPC Listening on %s", cfg.Server.Addr)
	log.Println("")
	srv := grpc.NewServer()
	return listener, srv
}

func initializeReflectionConnection(cfg *config.Config, server *grpc.Server, listener net.Listener) {
	reflection.Register(server)
	if err := server.Serve(listener); err != nil {
		log.Println(err)
		log.Panicf("Failed SERVE gRPC => %v", err)
	}
	log.Printf("RPC Listening on %s", cfg.Server.Addr)
	log.Println("")
}

func runGRPC(cmd *cobra.Command, args []string) error {
	//init context
	//todo:: uncomment for future use
	//ctx := context.Background()

	// initial config
	cfg := config.InitConfig()
	listener, server := initializeListener(&cfg)

	//init database connection
	dbSrvc, err := db.Open(&cfg.DB)
	if err != nil {
		log.Fatalln(err)
	}

	//init redis server
	//todo:: uncomment for future use
	//redisServer := redis.NewRedisServer(&cfg.Redis)
	//redisClient, err := redisServer.Connect(ctx)
	//if err != nil {
	//	log.Fatalln(err)
	//}

	// Initialize Mailer Service
	//todo: uncomment for future use
	//mailer := mailer.NewMailService(&cfg.Mailer)

	//init repositories
	repositoryCtx := repository.InitRepository(dbSrvc)
	//init services
	serviceCtx := service.InitService(repositoryCtx)
	//init handlers
	handlerCtx := handler.InitHandler(serviceCtx)
	registerHandler(handlerCtx)

	// End Initialize
	initializeReflectionConnection(&cfg, server, listener)
	return nil
}

func registerHandler(ctx *handler.HandlerCtx) {

}
