package mailer

import (
	"bytes"
	"fmt"
	"gopkg.in/gomail.v2"
	"html/template"
	"io"
	"kubik-api-admin/config"
	"kubik-api-admin/constants"
	"kubik-api-admin/object/mail"
	"kubik-api-admin/utils"
	"kubik-api-admin/utils/errors"
	"mime/multipart"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
)

type MailServiceInterface interface {
	Send(detail mail.MailDetail, files []string) error
	ParseAttachmentsFromMultipartForm(files []*multipart.FileHeader) ([]mail.MailAttachment, error)
}

func NewMailService(cfg *config.Mailer) MailServiceInterface {
	return &mailer{
		cfg: cfg,
	}
}

type mailer struct {
	cfg *config.Mailer
}

func (m *mailer) ParseAttachmentsFromMultipartForm(files []*multipart.FileHeader) ([]mail.MailAttachment, error) {
	results := []mail.MailAttachment{}

	for i := range files {
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			return results, errors.Wrap(err)
		}
		out, err := os.Create(fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, files[i].Filename))

		defer out.Close()
		if err != nil {
			return results, errors.Wrap(err)
		}
		_, err = io.Copy(out, file)
		if err != nil {
			return results, errors.Wrap(err)
		}
		results = append(results, mail.MailAttachment{
			FileDir:  fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, files[i].Filename),
			FileName: files[i].Filename,
		})
	}

	return results, nil
}

func (m *mailer) Send(detail mail.MailDetail, files []string) error {
	mainTemplate := constants.MailMainTemplate
	logoTemplate := constants.MailLogoTemplate
	footerTemplate := constants.MailFooterTemplate
	defaultBodyTemplate := constants.DefaultBodyTemplate

	var result string

	if detail.Template != "" && detail.Html != "" {
		log.Error(errors.Wrap(constants.ErrEmailTemplate))
		return errors.Wrap(constants.ErrEmailTemplate)
	}
	if detail.Template != "" {
		templ := template.New("").Funcs(template.FuncMap{
			"CurrencyFormat": utils.CommaSeparated,
		})
		t := template.Must(templ.ParseFiles(mainTemplate, logoTemplate, footerTemplate, detail.Template))
		var tpl bytes.Buffer
		if err := t.ExecuteTemplate(&tpl, "layout", detail.Data); err != nil {
			log.Error(err)
			return err
		}

		result = tpl.String()
	} else if detail.Html != "" {
		detail.Data = struct {
			Year int
		}{
			Year: time.Now().Year(),
		}

		t := template.Must(template.ParseFiles(mainTemplate, logoTemplate, footerTemplate, defaultBodyTemplate))
		var tpl bytes.Buffer
		if err := t.ExecuteTemplate(&tpl, "layout", detail.Data); err != nil {
			log.Error(err)
			return err
		}

		result = tpl.String()

		result = strings.ReplaceAll(result, "{BODY_TEMPLATE}", detail.Html)
	}
	result = strings.ReplaceAll(result, "{LOGO_DESI}", constants.KubikLogoURL)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", m.cfg.Sender)
	mailer.SetHeader("To", detail.To...)
	for _, cc := range detail.Cc {
		mailer.SetAddressHeader("Cc", cc, cc)
	}
	mailer.SetHeader("Subject", detail.Subject)
	mailer.SetBody("text/html", result)
	for _, v := range detail.Attachments {
		filePath, err := filepath.Abs(v.FileDir)
		if err != nil {
			log.Error(err)
			return err
		}
		mailer.Attach(filePath, gomail.Rename(v.FileName))
	}

	dialer := gomail.NewPlainDialer(
		m.cfg.Server,
		int(m.cfg.Port),
		m.cfg.Username,
		m.cfg.Password,
	)

	if err := dialer.DialAndSend(mailer); err != nil {
		log.Error(err)
		return err
	}

	if files != nil {
		for _, v := range files {
			err := os.Remove(fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, v))
			if err != nil {
				log.Error(err)
			}
		}
	}

	return nil
}
