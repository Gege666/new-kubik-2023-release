package middleware

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"github.com/go-redis/redis/v8"

	"kubik-api-admin/config"

	"github.com/dgrijalva/jwt-go"
)

//go:generate mockgen -source=./jwt.go -destination=./mocks/jwt/jwt.go -package=jwt_service_mocks

type JWTInterface interface {
	ExtractJWTClaims(ctx context.Context, authBearer string) (claims *JWTClaims, err error)
	ValidateTokenIssuer(claims *JWTClaims) (err error)
	ValidateTokenExpire(ctx context.Context, claims *JWTClaims, reqToken string) (err error)

	GetTokenFromRedis(ctx context.Context, id int, authKey string) (string, error)
	DeleteTokenFromRedis(ctx context.Context, id int, authKey string) error
}

const (
	AuthKeyUser = "desi-auth-user"
)

type jwtObj struct {
	config *config.JWTConfig
	redis  *redis.Client
}

func NewJWT(cfg *config.JWTConfig, redis *redis.Client) JWTInterface {
	return &jwtObj{
		config: cfg,
		redis:  redis,
	}
}

func (j *jwtObj) ExtractJWTClaims(ctx context.Context, token string) (claims *JWTClaims, err error) {
	// check authorization
	splitToken := strings.Split(token, bearer)
	if len(splitToken) != 2 {
		// TODO: change second nil
		return nil, nil
	}
	reqToken := strings.TrimSpace(splitToken[1])

	t, err := jwt.ParseWithClaims(reqToken, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		return j.config.Secret, nil
	})

	// if err != nil && err.Error() != constants.ErrKeyIsNotInvalidType.Error() {
	// 	return nil, err
	// }

	claims = t.Claims.(*JWTClaims)

	// Validate Issuer Token
	err = j.ValidateTokenIssuer(claims)
	if err != nil {
		return nil, err
	}

	// Validate token expire
	err = j.ValidateTokenExpire(ctx, claims, reqToken)
	if err != nil {
		return nil, err
	}
	return claims, nil
}

// ValidateTokenIssuer is for validate token issuer
func (j *jwtObj) ValidateTokenIssuer(claims *JWTClaims) (err error) {
	if claims.Issuer != j.config.Issuer {
		// TODO: change nil constants
		return nil
		// return constants.ErrTokenInvalid
	}
	return nil
}

// ValidateTokenExpire is for validate Token Expire
func (j *jwtObj) ValidateTokenExpire(ctx context.Context, claims *JWTClaims, reqToken string) (err error) {
	// check token to redis
	key := AuthKeyUser
	token, err := j.GetTokenFromRedis(ctx, claims.ID, key)
	if err != nil {
		return errors.New("TOKEN IS EXPIRED")
	}

	if token != reqToken {
		return errors.New("TOKEN IS REPLACED")
	}

	return nil
}

func (j *jwtObj) GetTokenFromRedis(ctx context.Context, id int, authKey string) (string, error) {
	key := fmt.Sprintf("%s:%d", authKey, id)
	val, err := j.redis.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}

	return val, nil
}

func (j *jwtObj) DeleteTokenFromRedis(ctx context.Context, id int, authKey string) error {
	key := fmt.Sprintf("%s:%d", authKey, id)
	_, err := j.redis.Del(ctx, key).Result()
	if err != nil {
		return err
	}

	return nil
}
