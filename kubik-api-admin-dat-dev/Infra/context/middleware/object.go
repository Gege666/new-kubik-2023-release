package middleware

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	bearer = "Bearer"
)

var (
	JWTSigningMethod = jwt.SigningMethodHS256
)

type JWTClaims struct {
	jwt.StandardClaims
	ID           int
	Email        string
	Name         string
	CostCenterID int
	JoinDate     *time.Time
	Role         string
}
