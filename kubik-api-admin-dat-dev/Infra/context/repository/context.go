package repository

import (
	"kubik-api-admin/infra/db"
)

type RepositoryCtx struct {
	DB *db.DB
}

func InitRepository(db *db.DB) *RepositoryCtx {
	return &RepositoryCtx{}
}
