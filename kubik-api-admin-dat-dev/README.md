# Kubik API Mobile

## Technology

- Golang `go 1.19`
- PostgresSQL
- Redis
- gRPC

## How to Start Develop

Before developing this project, you need to do some setup.

### Installing Golang
1. Download golang using this command
```
$ wget "https://dl.google.com/go/$(curl https://go.dev/VERSION?m=text).linux-amd64.tar.gz" -P ~/
```

2. Extract and place to system path
```
$ sudo su
$ rm -rf /usr/local/go && tar -C /usr/local -xzf go1.19.5.linux-amd64.tar.gz
$ exit
```

3. Add /usr/local/go/bin to the PATH environment variable.
You can do this by adding the following line to your $HOME/.profile or /etc/profile (for a system-wide installation):

```
export PATH=$PATH:/usr/local/go/bin
```
**Note:** Changes made to a profile file may not apply until the next time you log into your computer. To apply the changes immediately, just run the shell commands directly or execute them from the profile using a command such as source $HOME/.profile.

4. Verify that you've installed Go by opening a command prompt and typing the following command:

```
$ go version
```
5. Confirm that the command prints the installed version of Go.


### Installing Protocol Buffer Compiler
- Linux, using apt or apt-get, for example:
```
$ apt install -y protobuf-compiler
$ protoc --version  # Ensure compiler version is 3+
```

- MacOS, using Homebrew:
```
$ brew install protobuf
$ protoc --version  # Ensure compiler version is 3+
```

After that, install the compiler for gRPC version from go using this command:
```
$ go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
$ go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
```

Update your PATH so that the protoc compiler can find the plugins:
**Note:** if your go/bin path has been included on your .profile or .(bash/zsh/etc).rc file, you don't need to do this
```
$ export PATH="$PATH:$(go env GOPATH)/bin"
```

### Installing Redis
Currently on latest version of Ubuntu 22.04 (Jammy Jellyfish),
you can run the following command to install
```
$ sudo apt install redis-server -y
```

### Installing PostgreSQL
1. To install PostgreSQL, first refresh your server’s local package index:
```
$ sudo apt update
```
2. Then, install the Postgres package along with a -contrib package that adds some additional utilities and functionality:
```
$ sudo apt install postgresql postgresql-contrib
```
Ensure that the service is started:
```
$ sudo systemctl start postgresql.service
```
or you can use the service method:
```
$ sudo service postgresql start
```
**NOTE:** follow this step if you are struggling when using postgres usage, some user have issue for logging in for the first time due to postgres has a complex security on first setup.

1. Access the postgres default database for creating user and role
```
$ sudo -u postgres psql
```
2. After accessing the database, create new user to use:
```
postgres=# CREATE USER <Your User Name> WITH PASSWORD '<YourComplexPassword>';
```
3. Make your created user a superuser role for all access
```
postgres=# ALTER USER <YourUserName> WITH SUPERUSER;
ALTER ROLE
postgres=# \q;
```
4. Run this command **in terminal** for creating db for your newly created user
```
$ createdb
```
5. Then try to login to psql
```
$ psql -U <yourUserName>
```
6. Create database for this project
```
postgres=# CREATE DATABASE kubik_test
```

## Install All Go Package

```
$ make mod-get
$ make install
```

# Running Project

## Setting up the environment
Before running the project, make sure you have clone the example .env.yml.
```
cp .env.yml.example .env.yml
```

**NOTE:** After cloning the example, make sure you have  change the value for database and mailer, the other value is optional, you can change if you want to.
Value that need to be changed is: 
1. User and Password in database section
- host=XXX port=XXX user=**YourDatabaseUser** dbname=XXX password=**YourDatabasePassword** sslmode=disable TimeZone=XXX/XXX

2. mailer:
  server: "**YourMailerServiceURI**"
  port: **YourMailerServicePort**
  username: "**YourMailTrapUsername**"
  password: "**YourMailTrapPassword**"
  useTls: false (Depends on you, and your mailer service)
  sender: "XXX"
  maxAttempt: 5 (Depends on you, and your mailer service)

## Generating Protobuf

## Run HTTP API

```
$ make run-http
```

or if you want to use the hot reload run 

```
$ make hot-http
```

## Run gRPC API
```
$ make run-grpc
```

also you can do the same thing if you want to use hot reload, just change from http to grpc:

```
$ make hot-grpc
```

But when there is any new feature, please register the path into `Makefile`
