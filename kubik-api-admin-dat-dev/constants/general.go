package constants

// Date format constants
const (
	KubikLogoURL = ""

	Authorization         = "Authorization"
	DateTimeFormatStd     = "2006-01-02 15:04:05"
	DateFormatStd         = "2006-01-02"
	TimeFormatStd         = "15:04:05"
	TimeFormatShort       = "15:04"
	ENV_STAGING           = "STAGING"
	ENV_PRODUCTION        = "PROD"
	TempDownloadedFileDir = "./temp/"
	MailMainTemplate      = "templates/layout/Main.html"
	MailLogoTemplate      = "templates/layout/Logo.html"
	MailFooterTemplate    = "templates/layout/Footer.html"
	DefaultBodyTemplate   = "templates/layout/DefaultBody.html"
)
