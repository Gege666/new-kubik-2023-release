package constants

import (
	"errors"
)

const (
	PGDuplicateConstraint = "pq: duplicate key value violates unique constraint "

	ErrIsNotPrivateEvent = "Event with id %s is not private event"
)

var (
	ErrDataNotFound             = errors.New("Data Not Found")
	ErrInvalidEmailAddress      = errors.New("Invalid Email Address")
	ErrNameIsRequired           = errors.New("Name is required")
	ErrEmailIsRequired          = errors.New("Email is required")
	ErrPasswordIsRequired       = errors.New("Password is required")
	ErrOldPasswordIsRequired    = errors.New("Old Password is required")
	ErrNewPasswordIsRequired    = errors.New("New Password is required")
	ErrEmpNoIsRequired          = errors.New("Employee No is required")
	ErrPhoneIsRequired          = errors.New("HandPhone is required")
	ErrPasswordAlreadyTaken     = errors.New("New Password and Old Password should not be same")
	ErrEmailAndPasswordNotMatch = errors.New("Email or Password Not Match")
	ErrKeyIsNotInvalidType      = errors.New("key is of invalid type")
	ErrTokenIsRequired          = errors.New("Token is required")
	ErrTokenInvalid             = errors.New("Token is invalid")
	ErrPasswordNotMatch         = errors.New("Password doesn't match")
	ErrSaveTokenToRedis         = errors.New("Error Save Token to redis")
	ErrGetTokenFromRedis        = errors.New("Error Get Token from redis")
	ErrSaveOTPToRedis           = errors.New("Error Save OTP to redis")
	ErrGetOTPFromRedis          = errors.New("OTP is invalid or has been expired.")
	ErrDeleteOTPFromRedis       = errors.New("Error Delete OTP from redis")
	ErrFcmTokenIsRequired       = errors.New("FCM token is required")
	ErrDeviceIsRequired         = errors.New("Device is required")
	ErrUserFCMNotFound          = errors.New("User Fcm not found")
	ErrSaveOrUpdateFCM          = errors.New("Error Save or Update fcm token")
	ErrEmailNotFound            = errors.New("Email not found")
	ErrOldPasswordNotMatch      = errors.New("Old Password not match")
	ErrTokenAlreadyExpired      = errors.New("Token Already Expired")
	ErrTokenReplaced            = errors.New("Please re login for next process")
	ErrOTPIsRequired            = errors.New("OTP is required")
	ErrOTPNotMatch              = errors.New("OTP doesn't match")
	ErrValidation               = errors.New("Error Validation Request")
	ErrUserInactive             = errors.New("This account is at the moment suspended.")
	ErrUnchangedPassword        = errors.New("Please change your password first")
	ErrInvalidPassword          = errors.New("Password should contains upper case character, lower case characters, a digit and a special character.")
	ErrEmailTemplate            = errors.New("Email should have template and/or html body")
)
