package handler

import (
	loginlogout "kubik-api-auth/handlers/login_logout"
	"kubik-api-auth/handlers/registration"
	"kubik-api-auth/infra/context/service"
)

type HandlerCtx struct {
	LoginLogoutHandler  loginlogout.LoginLogoutHandler
	RegistrationHandler registration.RegistrationHandler
}

func InitHandler(ctx *service.ServiceCtx) *HandlerCtx {
	loginLogoutHandler := loginlogout.NewLoginLogoutHandler(ctx)
	registrationHandler := registration.NewRegistrationHandler(ctx)
	return &HandlerCtx{
		LoginLogoutHandler:  loginLogoutHandler,
		RegistrationHandler: registrationHandler,
	}
}
