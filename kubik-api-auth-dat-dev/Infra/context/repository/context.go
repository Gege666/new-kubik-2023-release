package repository

import (
	"kubik-api-auth/infra/db"
	"kubik-api-auth/repositories/user"
	"kubik-api-auth/repositories/user_token"
)

type RepositoryCtx struct {
	DB                  *db.DB
	UserRepository      user.UserRepository
	UserTokenRepository user_token.UserTokenRepository
}

func InitRepository(db *db.DB) *RepositoryCtx {
	userRepository := user.NewUserRepository(db)
	userTokenRepository := user_token.NewUserTokenRepository(db)
	return &RepositoryCtx{
		DB:                  db,
		UserRepository:      userRepository,
		UserTokenRepository: userTokenRepository,
	}
}
