package service

import (
	"kubik-api-auth/config"
	"kubik-api-auth/infra/context/repository"
	"kubik-api-auth/infra/mailer"
	"kubik-api-auth/infra/middleware"
	"kubik-api-auth/infra/otp"
	loginlogout "kubik-api-auth/services/login_logout"
	"kubik-api-auth/services/registration"
)

type ServiceCtx struct {
	LoginLogoutService  loginlogout.LoginLogoutService
	RegistrationService registration.RegistrationService
	Mailer              mailer.MailService
	OTPService          otp.OTPService
	JWTService          middleware.JWTService
	Config              *config.Config
}

func InitService(repositoryCtx *repository.RepositoryCtx, mailer mailer.MailService, otpService otp.OTPService, jwtService middleware.JWTService, config *config.Config) *ServiceCtx {
	loginLogoutService := loginlogout.NewLoginLogoutService(repositoryCtx, mailer, otpService, config, jwtService)
	registrationService := registration.NewRegistrationService(repositoryCtx, mailer, otpService)
	return &ServiceCtx{
		Mailer:              mailer,
		OTPService:          otpService,
		LoginLogoutService:  loginLogoutService,
		RegistrationService: registrationService,
		JWTService:          jwtService,
		Config:              config,
	}
}
