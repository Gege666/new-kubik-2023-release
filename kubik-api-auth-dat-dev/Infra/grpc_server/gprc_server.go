package grpc_server

import (
	"kubik-api-auth/config"
	"kubik-api-auth/infra/context"
	"log"
	"net"
	"os"

	"github.com/spf13/cobra"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

var (
	grpcCMD = &cobra.Command{
		Use:   "serve-grpc",
		Short: "Run grpc_server server",
		Long:  "Kubik API Auth",
		RunE:  runGRPC,
	}
)

func runGRPC(cmd *cobra.Command, args []string) error {
	// initial config
	cfg := config.InitConfig()
	// Initialize Repositories, Services, and Handlers and register GRPC server to handler here
	serverContext := context.NewGRPCServer(&cfg)

	// Initialize The Unary Interceptor
	serverOptions := serverContext.ServerOptions()

	// Initialize the listener for GRPC
	listener, server := initializeListener(&cfg, serverOptions)

	// Register the handler to server
	serverContext.RegisterHandler(server)

	// End Initialize
	initializeReflectionConnection(&cfg, server, listener)
	return nil
}

func initializeListener(cfg *config.Config, serverOptions []grpc.ServerOption) (net.Listener, *grpc.Server) {
	// Open and Listen Server to configured Address
	listener, err := net.Listen("tcp", cfg.Server.Addr)
	if err != nil {
		log.Panicf("Failed listen port %s => %v", cfg.Server.Addr, err)
		os.Exit(0)
	}

	log.Printf("RPC Listening on %s", cfg.Server.Addr)
	log.Println("")
	srv := grpc.NewServer(
		serverOptions...,
	)
	return listener, srv
}

func initializeReflectionConnection(cfg *config.Config, server *grpc.Server, listener net.Listener) {
	reflection.Register(server)
	if err := server.Serve(listener); err != nil {
		log.Println(err)
		log.Panicf("Failed SERVE gRPC => %v", err)
	}
	log.Printf("RPC Listening on %s", cfg.Server.Addr)
	log.Println("")
}

// ServeHTTP return instance of serve HTTP command object
func ServeGRPC() *cobra.Command {
	return grpcCMD
}
