package interceptor

import (
	"context"
	"kubik-api-auth/constants"
	"kubik-api-auth/infra/db"
	"kubik-api-auth/infra/middleware"
	"kubik-api-auth/objects/jwt"

	log "github.com/sirupsen/logrus"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

// AuthInterceptor config struct required by interceptor
type AuthInterceptor struct {
	JWTService middleware.JWTService
	Claims     *jwt.JWTClaims
	DB         *db.DB
}

// Unary gRPC interceptor
func (interceptor *AuthInterceptor) Unary(exceptions []string, appName string) grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		err := interceptor.authorize(ctx, info.FullMethod, exceptions, appName)
		if err != nil {
			return nil, err
		}
		return handler(context.WithValue(ctx, constants.ClaimsContextKey, interceptor.Claims), req)
	}
}

func (interceptor *AuthInterceptor) authorize(ctx context.Context, method string, exceptions []string, appName string) error {
	log.Println("--> unary authorize: ", method)
	for _, v := range exceptions {
		if v == method {
			return nil
		}
	}
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}
	// fmt.Println("md", md)

	values := md["authorization"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	claims, errs := interceptor.JWTService.ExtractJWTClaims(ctx, values[0], appName)
	if errs != nil {
		return status.Errorf(codes.Unauthenticated, errs.Err.Error())
	}

	timezone := md["timezone"]
	if len(timezone) > 0 {
		claims.Timezone = timezone[0]
	} else {
		claims.Timezone = "Asia/Jakarta"
	}

	companyID := md["companyid"]
	if len(companyID) > 0 {
		claims.CompanyID = companyID[0]
	} else {
		claims.CompanyID = ""
	}
	EmployeeID := md["employeeid"]
	if len(EmployeeID) > 0 {
		claims.EmployeeID = EmployeeID[0]
	} else {
		claims.EmployeeID = ""
	}

	interceptor.Claims = claims

	return nil
}
