package mailer

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"kubik-api-auth/config"
	"kubik-api-auth/constants"
	objects "kubik-api-auth/objects/mail"
	"kubik-api-auth/utils"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc/codes"
	"gopkg.in/gomail.v2"
)

type MailService interface {
	Send(detail objects.MailDetail, files []string) *constants.ErrorResponse
	ParseAttachmentsFromMultipartForm(files []*multipart.FileHeader) ([]objects.MailAttachment, *constants.ErrorResponse)
}

func NewMailService(cfg *config.Mailer) MailService {
	return &mailer{
		cfg: cfg,
	}
}

type mailer struct {
	cfg *config.Mailer
}

func (m *mailer) ParseAttachmentsFromMultipartForm(files []*multipart.FileHeader) ([]objects.MailAttachment, *constants.ErrorResponse) {
	results := []objects.MailAttachment{}

	for i := range files {
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			return results, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}
		out, err := os.Create(fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, files[i].Filename))

		defer out.Close()
		if err != nil {
			return results, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}
		_, err = io.Copy(out, file)
		if err != nil {
			return results, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}
		results = append(results, objects.MailAttachment{
			FileDir:  fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, files[i].Filename),
			FileName: files[i].Filename,
		})
	}

	return results, nil
}

func (m *mailer) Send(detail objects.MailDetail, files []string) *constants.ErrorResponse {
	mainTemplate := constants.MailMainTemplate
	logoTemplate := constants.MailLogoTemplate
	footerTemplate := constants.MailFooterTemplate
	defaultBodyTemplate := constants.DefaultBodyTemplate

	var result string

	if detail.Template != "" && detail.Html != "" {
		log.Error(constants.ErrEmailTemplate)
		return constants.ErrEmailTemplate
	}

	if detail.Template != "" {
		templ := template.New("").Funcs(template.FuncMap{
			"CurrencyFormat": utils.CommaSeparated,
		})
		t := template.Must(templ.ParseFiles(mainTemplate, logoTemplate, footerTemplate, detail.Template))
		var tpl bytes.Buffer
		if err := t.ExecuteTemplate(&tpl, "layout", detail.Data); err != nil {
			return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}

		result = tpl.String()
	} else if detail.Html != "" {
		detail.Data = struct {
			Year int
		}{
			Year: time.Now().Year(),
		}

		t := template.Must(template.ParseFiles(mainTemplate, logoTemplate, footerTemplate, defaultBodyTemplate))
		var tpl bytes.Buffer
		if err := t.ExecuteTemplate(&tpl, "layout", detail.Data); err != nil {
			return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}

		result = tpl.String()

		result = strings.ReplaceAll(result, "{BODY_TEMPLATE}", detail.Html)
	}
	result = strings.ReplaceAll(result, "{LOGO_URL}", detail.LogoURL)

	mailer := gomail.NewMessage()
	mailer.SetHeader("From", m.cfg.Sender)
	mailer.SetHeader("To", detail.To...)
	for _, cc := range detail.Cc {
		mailer.SetAddressHeader("Cc", cc, cc)
	}
	mailer.SetHeader("Subject", detail.Subject)
	mailer.SetBody("text/html", result)
	for _, v := range detail.Attachments {
		filePath, err := filepath.Abs(v.FileDir)
		if err != nil {
			log.Error(err)
			return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
		}
		mailer.Attach(filePath, gomail.Rename(v.FileName))
	}

	dialer := gomail.NewPlainDialer(
		m.cfg.Server,
		int(m.cfg.Port),
		m.cfg.Username,
		m.cfg.Password,
	)

	if err := dialer.DialAndSend(mailer); err != nil {
		log.Error(err)
		return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	if files != nil {
		for _, v := range files {
			err := os.Remove(fmt.Sprintf("%s%s", constants.TempDownloadedFileDir, v))
			if err != nil {
				log.Error(err)
			}
		}
	}

	return nil
}
