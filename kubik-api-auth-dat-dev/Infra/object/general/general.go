package general

type RequestByID struct {
	ID string
}

type RequestByEmail struct {
	Email string
}

type RequestByPhoneNumber struct {
	PhoneNumber string
}

type RequestByEmailOrPhoneNumber struct {
	Email       string
	PhoneNumber string
}

type RequestFormatGetList struct {
	Page   uint32
	Limit  uint32
	Search string
	Sort   string
	SortBy string
}

type Pagination struct {
	Page         uint32
	Limit        uint32
	Prev         uint32
	Next         uint32
	TotalPages   uint32
	TotalRecords uint32
}

type Meta struct {
	Message string
	Status  uint32
	Code    string
}

type LoginResponse struct {
	AccessToken          string
	RefreshToken         string
	FirestoreCustomToken string
}

func (r RequestByEmailOrPhoneNumber) GetValue() string {
	if r.Email == "" {
		return r.PhoneNumber
	}
	return r.Email
}
