package jwt

import "github.com/dgrijalva/jwt-go"

// JWTClaims struct of standard JWT middleware
type JWTClaims struct {
	jwt.StandardClaims
	ID          string
	Email       string
	Name        string
	RoleID      string
	FlutterUdid string
	Timezone    string
	TokenType   string
	Device      string
	IsAdmin     bool
	CompanyID   string
	EmployeeID  string
}kubik-api-auth-dat-dev

// JWTRequest struct for request jwt
type JWTRequest struct {
	ID          string `json:"id"`
	Name        string `json:"nama"`
	Email       string `json:"email"`
	RoleID      string `json:"role_id"`
	FlutterUdid string `json:"flutter_udid"`
	IsAdmin     bool   `json:"is_admin"`
	Timezone    string `json:"timezone"`
}

// JWTSimpleRequest struct for request jwt
type JWTSimpleRequest struct {
	UID    string               `json:"uid"`
	Claims JWTSimpleChildClaims `json:"claims"`
}

// JWTSimpleChildClaims struct for request jwt
type JWTSimpleChildClaims struct {
	UID string `json:"uid"`
	Alg string `json:"alg"`
}

// JWTSimpleClaims struct for claims response jwt
type JWTSimpleClaims struct {
	jwt.StandardClaims
	JWTSimpleRequest
}
