package login_logout

type RefreshTokenData struct {
	AccessToken  string
	RefreshToken string
}

type ChangePasswordRequest struct {
	UserId      string
	Otp         string
	NewPassword string
}

type UpdatePasswordRequest struct {
	UserId      string
	OldPassword string
	NewPassword string
}

type LoginByGoogleRequest struct {
	Token        string
	FcmToken     string
	Device       string
	FlutterUdid  string
	IsAdminLogin bool
}

type LoginByOTPRequest struct {
	Input        string
	Otp          uint32
	FcmToken     string
	Device       string
	FlutterUdid  string
	IsAdminLogin bool
}

type LoginByPasswordRequest struct {
	Input        string
	Password     string
	FcmToken     string
	Device       string
	FlutterUdid  string
	IsAdminLogin bool
}

type LoginRequestForTokenGeneration struct {
	Input        string
	Password     string
	OTP          uint32
	FcmToken     string
	Device       string
	FlutterUdid  string
	IsAdminLogin bool
}
