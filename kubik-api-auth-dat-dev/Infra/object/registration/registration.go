package registration

type Registration struct {
	Name        string
	Email       string
	Username    string
	PhoneNumber string
	Password    string
	// FlutterUdid string
	// Device      string
	// FcmToken    string
}

type RegistrationRequest struct {
	Name        string
	Email       string
	PhoneNumber string
	Password    string
}

type ValidationRequest struct {
	Email          string
	PhoneNumber    string
	ValidationCode string
}

func (r ValidationRequest) GetValue() string {
	if r.Email == "" {
		return r.PhoneNumber
	}
	return r.Email
}
