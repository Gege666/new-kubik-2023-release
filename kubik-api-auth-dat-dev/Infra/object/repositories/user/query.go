package user

const (
	getUserByEmailOrPhoneNumberQuery = `
		SELECT
			id,
			name,
			email,
			password,
			phone_number,
			email_verification_time,
			phone_number_verification_time,
			role_id
		FROM users
		WHERE email ILIKE $1 OR phone_number = $1 OR username = $1
	`

	getUserByID = `
		SELECT
			id,
			name,
			email,
			password,
			phone_number,
			email_verification_time,
			phone_number_verification_time,
			role_id
		FROM users
		WHERE id = $1
	`

	getUserByGoogleIDQuery = `
		SELECT
			id,
			name,
			email,
			password,
			phone_number,
			email_verification_time,
			phone_number_verification_time,
			role_id
		FROM users
		WHERE google_user_id = $1
	`

	registrationQuery = `
		INSERT INTO users (
			name,
			email,
			phone_number,
			password,
			google_user_id,
			email_verification_time,
			username,
			role_id
		) VALUES
		($1, $2, $3, $4, $5, $6, $7, $8)
		ON CONFLICT (email) DO UPDATE SET
			google_user_id = EXCLUDED.google_user_id
		RETURNING id
	`

	updateUserPasswordByID = `
		UPDATE users SET password = $2
		WHERE id = $1
	`

	validateUserEmailQuery = `
		UPDATE users SET email_verification_time = now()
		WHERE email ILIKE $1
	`

	validateUserPhoneNumberQuery = `
		UPDATE users SET phone_number_verification_time = now()
		WHERE id = $1
	`

	queryGetPhoneNumberVerification = `
		SELECT
			u.phone_number, u.phone_number_verification_time
		FROM users u
		WHERE u.id = $1
		LIMIT 1
	`
)
