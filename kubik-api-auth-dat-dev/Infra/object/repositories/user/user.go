package user

import (
	"context"
	"kubik-api-auth/constants"
	"kubik-api-auth/data/models/user"
	"kubik-api-auth/infra/db"
	"kubik-api-auth/utils"
	"net/http"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type userRepository struct {
	*db.DB
}

func (r userRepository) GetUserByEmailOrPhoneNumber(ctx context.Context, tx *sqlx.Tx, input string) (user.GetUser, *constants.ErrorResponse) {
	results := []user.GetUser{}
	err := tx.SelectContext(
		ctx,
		&results,
		getUserByEmailOrPhoneNumberQuery,
		input,
	)
	if err != nil {
		return user.GetUser{}, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}
	if len(results) == 0 {
		return user.GetUser{}, utils.ErrDataNotFound("users")
	}

	return results[0], nil
}
func (r userRepository) GetUserByID(ctx context.Context, tx *sqlx.Tx, userID string) (user.GetUser, *constants.ErrorResponse) {
	results := []user.GetUser{}

	err := tx.SelectContext(
		ctx,
		&results,
		getUserByID,
		userID,
	)
	if err != nil {
		return user.GetUser{}, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}
	if len(results) == 0 {
		return user.GetUser{}, utils.ErrDataNotFound("users")
	}

	return results[0], nil
}
func (r userRepository) GetUserByGoogleID(ctx context.Context, tx *sqlx.Tx, googleUserID string) (user.GetUser, *constants.ErrorResponse) {
	results := []user.GetUser{}

	err := tx.SelectContext(
		ctx,
		&results,
		getUserByGoogleIDQuery,
		googleUserID,
	)
	if err != nil {
		return user.GetUser{}, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}
	if len(results) == 0 {
		return user.GetUser{}, nil
	}

	return results[0], nil
}
func (r userRepository) UpdatePasswordByID(ctx context.Context, tx *sqlx.Tx, userID string, newPassword string) *constants.ErrorResponse {
	_, err := tx.ExecContext(
		ctx,
		updateUserPasswordByID,
		userID,
		newPassword,
	)
	if err != nil {
		return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	return nil
}
func (r userRepository) Registration(ctx context.Context, tx *sqlx.Tx, data user.Registration) (string, *constants.ErrorResponse) {
	var id string

	err := tx.QueryRowContext(
		ctx,
		registrationQuery,
		data.Name,
		data.Email,
		data.PhoneNumber,
		data.Password,
		data.GoogleUserID,
		data.EmailVerificationTime,
		data.Username,
		data.RoleID,
	).Scan(&id)
	if err != nil {
		errs := utils.ErrDuplicate(err.Error())
		if errs != nil {
			return id, errs
		}
		return id, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	return id, nil

}
func (r userRepository) ValidateUserEmail(ctx context.Context, tx *sqlx.Tx, email string) *constants.ErrorResponse {
	return nil
}
func (r userRepository) ValidateUserPhoneNumber(ctx context.Context, tx *sqlx.Tx, userID string) *constants.ErrorResponse {
	return nil
}
func (r userRepository) CheckPhoneNumberVerified(ctx context.Context, tx *sqlx.Tx, userID string) (string, bool, *constants.ErrorResponse) {
	return "", false, nil
}
