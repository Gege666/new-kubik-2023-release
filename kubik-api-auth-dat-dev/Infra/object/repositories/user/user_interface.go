package user

import (
	"context"
	"kubik-api-auth/constants"
	"kubik-api-auth/data/models/user"
	"kubik-api-auth/infra/db"

	"github.com/jmoiron/sqlx"
)

type UserRepository interface {
	GetUserByEmailOrPhoneNumber(ctx context.Context, tx *sqlx.Tx, input string) (user.GetUser, *constants.ErrorResponse)
	GetUserByID(ctx context.Context, tx *sqlx.Tx, userID string) (user.GetUser, *constants.ErrorResponse)
	GetUserByGoogleID(ctx context.Context, tx *sqlx.Tx, googleUserID string) (user.GetUser, *constants.ErrorResponse)
	UpdatePasswordByID(ctx context.Context, tx *sqlx.Tx, userID string, newPassword string) *constants.ErrorResponse
	Registration(ctx context.Context, tx *sqlx.Tx, data user.Registration) (string, *constants.ErrorResponse)
	ValidateUserEmail(ctx context.Context, tx *sqlx.Tx, email string) *constants.ErrorResponse
	ValidateUserPhoneNumber(ctx context.Context, tx *sqlx.Tx, userID string) *constants.ErrorResponse
	CheckPhoneNumberVerified(ctx context.Context, tx *sqlx.Tx, userID string) (string, bool, *constants.ErrorResponse)
}

func NewUserRepository(db *db.DB) UserRepository {
	return &userRepository{
		db,
	}
}
