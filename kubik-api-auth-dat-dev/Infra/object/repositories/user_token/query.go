package user_token

const (
	createUserToken = `
	INSERT INTO user_tokens (
		user_id,
		device,
		fcm_token,
		flutter_udid
	) VALUES
	($1, $2, $3, $4)
	ON CONFLICT (flutter_udid) DO UPDATE SET 
		user_id = EXCLUDED.user_id,
		device = EXCLUDED.device,
		fcm_token = EXCLUDED.fcm_token
`

	deleteUserTokenByFlutterUdidQuery = `
	DELETE FROM user_tokens 
	WHERE flutter_udid = $1
`

	getUserTokenByUserIDQuery = `
	SELECT
		user_id,
		device,
		fcm_token,
		flutter_udid,
		created_at
	FROM user_tokens
	WHERE user_id = $1
`
)
