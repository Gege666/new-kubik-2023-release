package user_token

import (
	"context"
	"kubik-api-auth/constants"
	"kubik-api-auth/data/models/user_token"
	"kubik-api-auth/infra/db"
	"net/http"

	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
)

type userTokenRepository struct {
	*db.DB
}

func (u userTokenRepository) CreateUserToken(ctx context.Context, tx *sqlx.Tx, data user_token.CreateUserToken) *constants.ErrorResponse {
	_, err := tx.ExecContext(
		ctx,
		createUserToken,
		data.UserID,
		data.Device,
		data.FcmToken,
		data.FlutterUDID,
	)
	if err != nil {
		return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	return nil
}

func (u userTokenRepository) GetUserTokenByUserID(ctx context.Context, tx *sqlx.Tx, userID string) ([]user_token.GetUserTokenByUserID, *constants.ErrorResponse) {
	results := []user_token.GetUserTokenByUserID{}

	err := tx.SelectContext(
		ctx,
		&results,
		getUserTokenByUserIDQuery,
		userID,
	)
	if err != nil {
		return nil, constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	return results, nil
}

func (u userTokenRepository) DeleteUserTokenByFlutterUdid(ctx context.Context, tx *sqlx.Tx, flutterUdid string) *constants.ErrorResponse {
	_, err := tx.ExecContext(
		ctx,
		deleteUserTokenByFlutterUdidQuery,
		flutterUdid,
	)
	if err != nil {
		return constants.Error(http.StatusInternalServerError, codes.Internal, constants.DefaultCustomErrorCode, err.Error())
	}

	return nil
}
