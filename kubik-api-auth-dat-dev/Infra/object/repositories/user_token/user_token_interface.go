package user_token

import (
	"context"
	"kubik-api-auth/constants"
	"kubik-api-auth/data/models/user_token"
	"kubik-api-auth/infra/db"

	"github.com/jmoiron/sqlx"
)

type UserTokenRepository interface {
	CreateUserToken(ctx context.Context, tx *sqlx.Tx, data user_token.CreateUserToken) *constants.ErrorResponse
	GetUserTokenByUserID(ctx context.Context, tx *sqlx.Tx, userID string) ([]user_token.GetUserTokenByUserID, *constants.ErrorResponse)
	DeleteUserTokenByFlutterUdid(ctx context.Context, tx *sqlx.Tx, flutterUdid string) *constants.ErrorResponse
}

func NewUserTokenRepository(db *db.DB) UserTokenRepository {
	return userTokenRepository{
		DB: db,
	}
}
