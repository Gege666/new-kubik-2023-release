package otp

import (
	"context"
	"kubik-api-auth/constants"

	"github.com/go-redis/redis/v8"
)

type OTPService interface {
	GenerateOTP(ctx context.Context, length int, hour int, id string, purpose string) (string, *constants.ErrorResponse)
	SaveOTPToRedis(ctx context.Context, id string, purpose string, hour int, otp string) *constants.ErrorResponse
	GetOTPFromRedis(ctx context.Context, id string, purpose string) (OTPData, *constants.ErrorResponse)
	DeleteOTPFromRedis(ctx context.Context, id string, purpose string) *constants.ErrorResponse
}

func NewOTPService(redis *redis.Client) OTPService {
	return &otpService{
		redis:   redis,
		appName: constants.GeneralAppName,
	}
}
