package context

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"kubik-api-auth/config"
	"kubik-api-auth/constants"
	loginlogout "kubik-api-auth/handlers/login_logout"
	"kubik-api-auth/handlers/registration"
	"kubik-api-auth/infra/context/handler"
	"kubik-api-auth/infra/context/repository"
	"kubik-api-auth/infra/context/service"
	"kubik-api-auth/infra/db"
	"kubik-api-auth/infra/interceptor"
	"kubik-api-auth/infra/mailer"
	"kubik-api-auth/infra/middleware"
	"kubik-api-auth/infra/otp"
	"kubik-api-auth/infra/redis"
	"log"
	"os"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var serverOptions []grpc.ServerOption

type contextInfra struct {
	cfg            *config.Config
	handlerContext *handler.HandlerCtx
	interceptor    interceptor.AuthInterceptor
}

type GRPCServer interface {
	RegisterHandler(server *grpc.Server)
	ServerOptions() []grpc.ServerOption
}

func NewGRPCServer(cfg *config.Config) GRPCServer {
	//	Initialize Context
	ctx := context.Background()

	// Initialize Database Connection
	dbSrvc, err := db.Open(&cfg.DB)
	if err != nil {
		log.Fatalln(err)
	}

	//	Initialize Redis
	redisServer := redis.NewRedisServer(&cfg.Redis)
	redisClient, err := redisServer.Connect(ctx)
	if err != nil {
		log.Fatalln(err)
	}

	// Initialize Mailer Service
	mailer := mailer.NewMailService(&cfg.Mailer)

	// Initialize Mailer Service
	OTPService := otp.NewOTPService(redisClient)

	// Initialize JWT Service
	jwtService := middleware.NewJWTService(&cfg.JWTConfig, redisClient)

	//	Initialize Repository Context
	repositoryCtx := repository.InitRepository(dbSrvc)

	//	Initialize Service Context
	serviceCtx := service.InitService(repositoryCtx, mailer, OTPService, jwtService, cfg)
	interceptor := interceptor.AuthInterceptor{
		JWTService: jwtService,
		DB:         dbSrvc,
	}
	//	Initialize Handler Context
	handlerCtx := handler.InitHandler(serviceCtx)
	return &contextInfra{
		cfg:            cfg,
		handlerContext: handlerCtx,
		interceptor:    interceptor,
	}
}

// GRPC Handler Registrar
// Register your handler to GRPC server here!
func (c contextInfra) RegisterHandler(server *grpc.Server) {
	loginlogout.RegisterLoginLogoutHandlerServer(server, c.handlerContext.LoginLogoutHandler)
	registration.RegisterRegisterHandlerServer(server, c.handlerContext.RegistrationHandler)
}

func (c contextInfra) ServerOptions() []grpc.ServerOption {
	c.initializeInterceptor()
	c.initializeTLSConnection()
	return serverOptions
}

func (c contextInfra) initializeInterceptor() {
	serverOptions = append(serverOptions, grpc.UnaryInterceptor(c.interceptor.Unary(constants.GrpcInterceptorException(), constants.GeneralAppName)))
}

func (c contextInfra) initializeTLSConnection() {
	if c.cfg.Server.ForceTLS {
		serverOptions = append(serverOptions, c.loadTLSCredentials())
	}
}

func (c contextInfra) loadTLSCredentials() grpc.ServerOption {
	log.Println("Loading certificate....")
	pemClientCA, err := os.ReadFile(constants.CertificateAuthorityDirectory)
	if err != nil {
		log.Fatal("cannot load TLS credentials: ", err)
		os.Exit(0)
	}

	certPool := x509.NewCertPool()
	if !certPool.AppendCertsFromPEM(pemClientCA) {
		log.Fatal("cannot load TLS credentials: ", err)
		os.Exit(0)
	}

	serverCert, err := tls.LoadX509KeyPair(constants.ServerCertificateDirectory, constants.ServerKeyDirectory)
	if err != nil {
		log.Fatal("cannot load TLS credentials: ", err)
		os.Exit(0)
	}

	config := &tls.Config{
		Certificates: []tls.Certificate{serverCert},
		ClientAuth:   tls.RequireAndVerifyClientCert,
		ClientCAs:    certPool,
	}
	log.Println("Certificate Loaded")
	return grpc.Creds(credentials.NewTLS(config))
}
