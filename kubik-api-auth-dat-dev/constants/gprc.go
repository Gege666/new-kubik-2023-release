package constants

const (
	GRPCRefreshTokenInvoker    = "/api_auth.LoginLogoutHandler/RefreshToken"
	GRPCUpdatePasswordInvoker  = "/api_auth.LoginLogoutHandler/UpdatePassword"
	GRPCLogoutInvoker          = "/api_auth.LoginLogoutHandler/Logout"
	GRPCChangePasswordInvoker  = "/api_auth.LoginLogoutHandler/ChangePassword"
	GRPCResetPasswordInvoker   = "/api_auth.LoginLogoutHandler/ResetPassword"
	GRPCLoginByGoogleInvoker   = "/api_auth.LoginLogoutHandler/LoginByGoogle"
	GRPCRequestOtpLoginInvoker = "/api_auth.LoginLogoutHandler/RequestOtpLogin"
	GRPCLoginByOtpInvoker      = "/api_auth.LoginLogoutHandler/LoginByOtp"
	GRPCLoginByPasswordInvoker = "/api_auth.LoginLogoutHandler/LoginByPassword"
)

// GrpcInterceptorException lists interceptor exception for gRPC
func GrpcInterceptorException() []string {
	return []string{
		GRPCChangePasswordInvoker,
		GRPCResetPasswordInvoker,
		GRPCLoginByGoogleInvoker,
		GRPCRequestOtpLoginInvoker,
		GRPCLoginByOtpInvoker,
		GRPCLoginByPasswordInvoker,
	}
}

const (
	CertificateAuthorityDirectory = "cert/ca-cert.pem"
	ServerCertificateDirectory    = "cert/server-cert.pem"
	ServerKeyDirectory            = "cert/server-key.pem"
)
