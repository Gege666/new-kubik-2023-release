package constants

// Mail templates
const (
	DefaultMailName = "layout"

	MailMainTemplate    = "templates/layout/Main.html"
	MailLogoTemplate    = "templates/layout/Logo.html"
	MailFooterTemplate  = "templates/layout/Footer.html"
	DefaultBodyTemplate = "templates/layout/DefaultBody.html"
)

const (
	// LogoURL url for Kubik
	LogoURL = "https://firebasestorage.googleapis.com/v0/b/kubik-web-app.appspot.com/o/logo2.png?alt=media&token=f64a30bd-77e3-4a3b-ba9a-63d601597fc5"
)

const (
	// MailRegistrationSubject subject for mail after registration
	MailRegistrationSubject = "Kubik - Account Activation"
	// MailLoginViaOTPSubject subject for mail after registration
	MailLoginViaOTPSubject = "Kubik - Login"
	// MailSendForgotPasswordOTPSubject subject for mail when user forgot password
	MailSendForgotPasswordOTPSubject = "Kubik - Forgot Password"
)

const (
	// MailRegistrationTemplate template for mail after registration
	MailRegistrationTemplate = "templates/page/registration.html"
	// MailLoginViaOTPTemplate template for mail after registration
	MailLoginViaOTPTemplate = "templates/page/login.html"
	// MailSendForgotPasswordOTPTemplate template for mail when user forgot password
	MailSendForgotPasswordOTPTemplate = "templates/page/forgot_password.html"
)
