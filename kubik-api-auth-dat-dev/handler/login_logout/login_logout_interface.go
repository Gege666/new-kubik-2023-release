package login_logout

import (
	context "context"
	"kubik-api-auth/infra/context/service"
	protobuf "kubik-api-auth/protobuf"
)

type LoginLogoutHandler interface {
	mustEmbedUnimplementedLoginLogoutHandlerServer()
	RefreshToken(ctx context.Context, in *protobuf.RequestById) (*RefreshTokenResponse, error)
	ResetPassword(ctx context.Context, in *protobuf.RequestByEmailOrPhone) (*ResetPasswordResponse, error)
	ChangePassword(ctx context.Context, in *ChangePasswordRequest) (*ChangePasswordResponse, error)
	UpdatePassword(ctx context.Context, in *UpdatePasswordRequest) (*UpdatePasswordResponse, error)
	Logout(ctx context.Context, in *protobuf.RequestById) (*LogoutResponse, error)
	LoginByGoogle(ctx context.Context, in *LoginByGoogleRequest) (*LoginResponse, error)
	RequestOtpLogin(ctx context.Context, in *protobuf.RequestByEmailOrPhone) (*OTPLoginResponse, error)
	LoginByOtp(ctx context.Context, in *LoginByOTPRequest) (*LoginResponse, error)
	LoginByPassword(ctx context.Context, in *LoginByPasswordRequest) (*LoginResponse, error)
}

func NewLoginLogoutHandler(ctx *service.ServiceCtx) LoginLogoutHandler {
	return &loginLogoutHandler{
		ServiceCtx: ctx,
	}
}
