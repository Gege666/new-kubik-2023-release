package registration

import (
	context "context"
	"kubik-api-auth/constants"
	"kubik-api-auth/infra/context/service"
	generalObjects "kubik-api-auth/objects/general"
	"kubik-api-auth/objects/registration"
	protobuf "kubik-api-auth/protobuf"
	"kubik-api-auth/utils"
	"net/http"
)

type registrationHandler struct {
	*service.ServiceCtx
}

func (r registrationHandler) mustEmbedUnimplementedRegisterHandlerServer() {}
func (r registrationHandler) Register(ctx context.Context, in *RegistrationRequest) (*RegistrationResponse, error) {
	var result RegistrationResponse
	_, err := r.RegistrationService.Register(ctx, registration.Registration{
		Name:        in.GetName(),
		Email:       in.GetEmail(),
		Username:    in.GetName(),
		PhoneNumber: in.GetPhoneNumber(),
		Password:    in.GetPassword(),
	}, "")

	if err != nil {
		utils.PrintError(*err)
		err := utils.GenerateGrpcError(err)

		return &result, err
	}

	result = RegistrationResponse{
		Meta: &protobuf.Meta{
			Message: "Registration",
			Status:  http.StatusCreated,
			Code:    constants.SuccessCode,
		},
		RegistrationMessage: "Account has been registered successfully",
	}
	return &result, nil
}

func (r registrationHandler) RequestForValidation(ctx context.Context, in *protobuf.RequestByEmailOrPhone) (*ValidationResponse, error) {
	var result ValidationResponse
	var message string
	var err *constants.ErrorResponse
	request := generalObjects.RequestByEmailOrPhoneNumber{
		Email:       in.GetEmail(),
		PhoneNumber: in.GetPhoneNumber(),
	}
	message, err = r.RegistrationService.RequestForValidation(ctx, request)
	if err != nil {
		utils.PrintError(*err)
		err := utils.GenerateGrpcError(err)
		return &result, err
	}
	result = ValidationResponse{
		Meta: &protobuf.Meta{
			Message: "Validation",
			Status:  http.StatusCreated,
			Code:    constants.SuccessCode,
		},
		ValidationMessage: message,
	}
	return &result, nil
}

func (r registrationHandler) Validate(ctx context.Context, in *ValidationRequest) (*ValidationResponse, error) {
	var result ValidationResponse
	var message string
	var err *constants.ErrorResponse
	var request registration.ValidationRequest
	request.Email = in.GetEmail()
	request.PhoneNumber = in.GetPhoneNumber()
	request.ValidationCode = in.GetValidationCode()
	message, err = r.RegistrationService.Validate(ctx, request)
	if err != nil {
		utils.PrintError(*err)
		err := utils.GenerateGrpcError(err)
		return &result, err
	}

	result = ValidationResponse{
		Meta: &protobuf.Meta{
			Message: "Validation",
			Status:  http.StatusCreated,
			Code:    constants.SuccessCode,
		},
		ValidationMessage: message,
	}
	return &result, nil
}
