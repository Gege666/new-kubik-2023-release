package registration

import (
	context "context"
	"kubik-api-auth/infra/context/service"
	protobuf "kubik-api-auth/protobuf"
)

type RegistrationHandler interface {
	mustEmbedUnimplementedRegisterHandlerServer()
	Register(context.Context, *RegistrationRequest) (*RegistrationResponse, error)
	RequestForValidation(context.Context, *protobuf.RequestByEmailOrPhone) (*ValidationResponse, error)
	Validate(context.Context, *ValidationRequest) (*ValidationResponse, error)
}

func NewRegistrationHandler(ctx *service.ServiceCtx) RegistrationHandler {
	return &registrationHandler{
		ServiceCtx: ctx,
	}
}
